//
//  AppDelegate.h
//  MyFirstApplication
//
//  Created by Sharath Kolibailu Venkatesh on 1/31/17.
//  Copyright © 2017 Sharath Kolibailu Venkatesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

