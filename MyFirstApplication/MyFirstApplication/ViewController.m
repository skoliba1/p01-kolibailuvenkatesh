//
//  ViewController.m
//  MyFirstApplication
//
//  Created by Sharath Kolibailu Venkatesh on 1/31/17.
//  Copyright © 2017 Sharath Kolibailu Venkatesh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (IBAction)clickMeButton:(id)sender;
- (IBAction)previousButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UIButton *submit;
@property (weak, nonatomic) IBOutlet UIImageView *tyImage;
@property (weak, nonatomic) IBOutlet UIButton *previous;

@property (nonatomic) int counter;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //_counter = 1;
    self.label1.text = @"";
    self.tyImage.image = [UIImage imageNamed: @"welcome.jpg"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)myEvent{
    
    switch(self.counter)
    {
        case 0 : self.tyImage.image = [UIImage imageNamed:@"welcome.jpg"];
            self.label1.text = @"";
            self.submit.hidden = false;
            self.previous.hidden = true;
            break;
            
        case 1 :self.tyImage.image = [UIImage imageNamed:@"1.jpg"];
            self.previous.hidden = false;
            break;
            
        case 2 :self.tyImage.image = [UIImage imageNamed:@"2.jpg"];
            break;
            
        case 3 : self.tyImage.image = [UIImage imageNamed:@"3.jpeg"];
            break;
            
        case 4 : self.tyImage.image = [UIImage imageNamed:@"4.jpg"];
            break;
            
        case 5 : self.tyImage.image = [UIImage imageNamed:@"5.jpg"];
            break;
            
        case 6 : self.tyImage.image = [UIImage imageNamed:@"6.jpeg"];
            break;
            
        default: self.tyImage.image = [UIImage imageNamed:@"tyImage.jpg"];
            self.label1.text = @"Thank you ! It was great time with you :)";
            self.submit.hidden = true;
            self.counter--;
            break;
            
    }

}
- (IBAction)clickMeButton:(id)sender {
    
    
    self.label1.text = @"Hello World! Here are some pictures for you. Click me again. :)";
    self.counter++;
    NSLog(@"%d",self.counter);
    NSLog(@"You clicked me !! Great !");
    [self myEvent];

}

- (IBAction)previousButton:(id)sender{
    if(self.counter > 0)
    {
        self.counter--;
        NSLog(@"%d", self.counter);
        self.label1.text = @"Hello World! Here are some pictures for you. Click me again. :)";
        [self myEvent];
        self.submit.hidden = false;
    }
    else
        self.previous.hidden = true;
    
}
@end
